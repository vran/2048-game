#!/bin/bash

apt-get update
apt-get install mc -y

cat /vagrant/.ssh/id_ed25519.pub >> /home/vagrant/.ssh/authorized_keys
cat /vagrant/.ssh/ansible_key.pub >> /home/vagrant/.ssh/authorized_keys