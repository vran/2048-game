# -*- mode: ruby -*-
# vi: set ft=ruby :
require "./vagrant_vars.rb"
include Variables

####################################### check errors #######################################
if VM_COUNT <= 0
  abort "Incorrect number of virtual machines. Variable VM_COUNT must be greater than 0."
end
if VM_HOSTS.size == 0
  abort "Variable VM_HOSTS is empty."
end
VM_HOSTS.each do |check|
  if check.respond_to?(:empty?) && check.empty? 
    abort "Some elements of VM_HOSTS is empty."
  end
end
if NEED_MGMT_VM == true
  if MGMT_VM_HOSTNAME == ""
    MGMT_VM_HOSTNAME = "management_host" + time.year + time.month + time.day + "-" + time.hour + time.min + time.sec
  end
  if VM_COUNT > VM_HOSTS.size + 1
    abort "Incorrect number of virtual machines. Variable VM_COUNT is greater than number of hosts in VM_HOSTS."
  end
else
  if VM_COUNT > VM_HOSTS.size
    abort "Incorrect number of virtual machines. Variable VM_COUNT is greater than number of hosts in VM_HOSTS."
  end
end

####################### define standard virtual maschines parameters #######################
def vm_create(config, hostname, ip_address, ports, script)
  config.vm.define hostname do |host|
    host.vm.provider "virtualbox" do |vb|
      vb.name = hostname
      vb.cpus = "#{VM_CPU_CORES}"
      vb.memory = "#{VM_MEMORY}"
    end
    host.vm.hostname = hostname
    host.vm.network "#{NETWORK_TYPE}_network", ip: ip_address
    if (ports != [])
      for port_forward in 0..(ports.size - 1)
        port = ports[port_forward].split(":")
        host.vm.network "forwarded_port", guest: port[0], host:port[1]
      end
    end
    host.vm.provision "shell", path: script
  end
end

################### create virtual maschines with additional parameters ####################
Vagrant.configure("2") do |config|
  config.vm.box = "#{OS_BOX}"
  config.vm.box_check_update = false
  config.vm.boot_timeout = 900
  for count in 1..VM_COUNT
    ports = []
    if (count == VM_COUNT) && (NEED_MGMT_VM == true)
      vm_create(config, MGMT_VM_HOSTNAME, "#{IP_RANGE}.10", [], "mgmt_script.sh")
    else
      if VM_HOSTS[count - 1].size > 1
        for ports_count in 1..(VM_HOSTS[count - 1].size - 1)
          ports.push(VM_HOSTS[count - 1][ports_count])
        end
      end
      vm_create(config, VM_HOSTS[count - 1][0], "#{IP_RANGE}.#{10 + count}", ports, "vm_script.sh")
    end
  end
end
