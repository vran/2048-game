# variables for vagrant

module Variables

# number of virtual maschines include ansible host
  VM_COUNT = 4
# number of cpu cores for each virtual maschine
  VM_CPU_CORES = 2
# amount of memory for each virtual machine
  VM_MEMORY = 2048
# network type for project ("private" or "public")
  NETWORK_TYPE = "private"
# ip subnet: first three octets with delimiters
  IP_RANGE = "192.168.100"
# set OS box
  OS_BOX = "debian/bullseye64"
# "true" if you need management host in this project
  NEED_MGMT_VM = false
# management host name
  MGMT_VM_HOSTNAME = "management-2048"
# list of vm names and forwarding ports except ansible. must to be specified as:
# [["vm1-name", "guest-port1:host-port1", .., "guest-portN:host-portN"],
# ..,
# ["vmN-name", "guest-port1:host-port1", .., "guest-portN:host-portN"]]
  VM_HOSTS = [
    ["game01-2048"],
    ["game02-2048"],
    ["game03-2048"],
    ["proxy01-2048", "80:80"]
  ]

end
