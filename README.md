Развертывание приложения game-2048
==================================

О проекте
---------
Приложение представляет из себя классическую игру "2048". Репозиторий с исходным кодом и описанием игры тут: <https://gitfront.io/r/deusops/JnacRhR4iD8q/2048-game/>  
Приложение запускается в docker-контейнере с Nginx в качесте веб-сервера.  
Разделение на dev, prod и другие контура в данном проекте не предусмотрено.  
Развертывание производится по двум сценариям:  
* На виртуальную машину или bare-metal - с помощью Ansible.  
* В кластере Kubernetes на Yandex-Cloud - с помощью Terraform и Helm.  

Структура проекта
-----------------
Создание локального окружения:  
* **Vagrantfile** содержит проверки и код для запуска виртуальных машин.  
* **vagrant_vars.rb** содержит переменные и их описания.  
* **mgmt_script.sh**, **vm_script.sh** - shell-скрипты, который выполняется при первом запуске виртуальных машин.  

Сборка docker-контейнера
------------------------
В директории **docker** находятся файлы для сборки образа с приложением. Сборка производится в multistage режиме. Сперва на основе node.js собирается веб-приложение. Далее оно копируется в контейнер с Nginx.  

Сценарий развертывани с помощью Ansible
---------------------------------------
Сценарий предполагает запуск контейнеров с приложением на нескольких хостах. Балансировка осуществляется с помощью сервиса haproxy на отдельном хосте.  

На управляющем хосте ansible должно быть установлено ПО (при использовании Vagrant будет установлено автоматически):  
* python 3  
* ansible >= 2.14  
* git
* роль docker: <https://gitlab.com/ansible_roles_v/docker>
* роль haproxy: <https://gitlab.com/ansible_roles_v/haproxy>
* коллекция community.docker для Ansible

Файлы для запуска этого сценария находятся в директории **ansible**:  
* **ansible.yml** - playbook для подготовки ansible на управляющем хосте.  
* **ansible.cfg** - пример конфигурационного файла для ansible.  
* **requirements.yml** - описание зависимостей для подготовки ansible на управляющем хосте.  
* **playbook.yml** playbook для развертывания окружения на хостах.  
* **inventories** - тут располагаются файлы hosts и переменные для запуска сценария.  
* **inventories/templates** - директория с шаблонами файлов конфигураций сервисов на хостах.  

Для корректной работы нужно указать значения переменных в файле *all.yml*.  
Переменные в файлах *game.yml* и *proxy.yml* обычно менять не требуется.

Сценарий развертывани в облаке с помощью Terraform
--------------------------------------------------
Сценарий предполагает создание кластера Managed Kubernetes в Yandex Cloud. Деплой приложения в кластер Kubernetes выполняется с помощью helm-чарта. Предусмотрено горизонтальное (*scale_policy*) и вертикальное масштабирование (*HorizontalPodAutoscaler*). Доступ к разворачиваемому приложению извне осуществляется с помощью контроллера *ingress-nginx*.  

Все нужные файлы находятся в директории **terraform**:  
* ***.tf** - манифесты для создания окружения и настройки кластера.  
* **helm/game** - helm-чарт для деплоя приложения в кластере.  
* **helm/ingress** - helm-чарты переменные для ingress-контроллера.  
* **backend.conf.example** - образец файла с ключами для бэкенда.  
* **meta.yml.example** - образец файла с настройкой подключения к кластеру Kubernetes.  
Перед запуском нужно в файле *99-variables.tf* указать свои значения переменных: *cloud_id*, *folder_id*, *whitelist_ip*, *management_ip* и при необходимости - других.  

CI/CD
-----
Для создания docker-образа и развертывания используются инструменты GitLab CI/CD.  
Сперва выполняются проверки файлов сценария Ansible, манифесты Terraform и линтер-тест helm-чарта. Для сокращения времени развертывания тестирование будет выполняться только в случае, если изменялись файлы в соответствующих директориях.  

Далее запускается деплой с помощью Absible и/или Terraform. Во избежание затирания и/или порчи настроек существующей инфраструктуры запуск развертывания выполняется вручную.  
Для обеспечения гибкости пайплайн Gitlab CI написан с подключением отдельных модулей: <https://gitlab.com/ci-modules>  
GitLab runner установлен на выделенный хост с помощью роли <https://gitlab.com/ansible_roles_v/gitlab-runner>  
Gitlab runner должен иметь доступ к мастер-хосту кластера по SSH. У пользователя, под которым выполняется подключение к мастер-хосту, должны быть права на управление кластером.  
Для тестирования используется готовый образ с преднастроенным Ansible-Linter: <https://registry.gitlab.com/docker_images_v/ansible-lint>.  
Для запуска сценария настройки конфигураций используется готовый образ с Ansible: <https://registry.gitlab.com/docker_images_v/ansible>.  

В настройках CI/CD нужно создать такие переменные:  
* ANSIBLE_USER - Простая переменная. Имя пользователя, под которым запускается playbook.  
* SSH_KEY - Файловая переменная. Ключ, с которым Ansible будет подключаться к целевым хостам.  
* HOSTS - Файловая переменная. Содержимое файла *ansible/inventories/hosts*.  
* CREDENTIALS - Файловая переменная. В этом проекте ее следует оставить пустой. Нужна для совместимости с модулем ci-modules/deploy-ansible.  
* META - Файловая переменная. Содержимое файла *terraform/meta.yml*. Настройки для подключения и управления кластером.  
* BACKEND_CONF - Файловая переменная. Содержимое файла *terraform/backend.conf*. Ключи для подключения и настройки облачного окружения.  
* YC_KEY - Файловая переменная. Содержимое файла *terraform/key.json*. Настройки для работы Yandex Cloud CLI.  
* TF_VAR_cloud_id - Простая переменная. ID облака в Yabdex Cloud.  
* TF_VAR_folder_id - Простая переменная. ID каталога в Yabdex Cloud.  
* TF_VAR_management_ip - Простая переменная. IP хоста, с которого разрешается подключаться по SSH к виртуальным машинам в YC.  
* TF_VAR_management_port - Простая переменная. Порт для подключения по SSH к виртуальным машинам в YC.  
Примеры файловых переменных есть в соответствующих файлах **.example*.  
Другие используемые переменные для CI/CD:  
* APPLICATION_NAME - Имя приложения (контейнера и образа).  
* APPLICATION_REPO - Репозиторий с docker-образом.  
* DOCKER_DIR - Расположение директории с файлами для сборки docker-образа. Если не указано, то будет выбрана текущая директория.  
* TERRAFORM_DIR - директория с манифестами Terraform. Если не указано, то будет выбрана текущая директория.  
* HELM_CHART_DIR - Расположение helm-чарта для деплоя приложения. Если не указано, то будет выбрана текущая директория.  
* TERRAFORM_USE_BACKEND_CONF - Логическая переменная. Указывает, нужно ли инициализировать Terraform с применением файла конфигурации *backend.conf*.  
* TF_VAR_bucket_state - Имя хранилища S3, где будет лежать файл состояния Terraform.  

Благодарности
-------------
* [Дипеж](https://gitlab.com/d3pre5s) за знания, науку, код-ревью и пр.  
* [Alexey Pavlov](https://gitlab.com/ap812) за помощь с тестированием и коментарии.