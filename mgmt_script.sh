#!/bin/bash

apt-get update
apt-get install mc python3-pip libssl-dev rsync git curl ansible -y
mkdir /etc/ansible
cp /vagrant/ansible/ansible.cfg /etc/ansible/ansible.cfg
cp -r /vagrant/.ssh/id_ed25519* /home/vagrant/.ssh
chown vagrant:vagrant /home/vagrant/.ssh/id_ed25519
chown vagrant:vagrant /home/vagrant/.ssh/id_ed25519.pub
chmod 600 /home/vagrant/.ssh/id_ed25519
chmod 644 /home/vagrant/.ssh/id_ed25519.pub
ansible-galaxy install -r /vagrant/ansible/requirements.yml --force