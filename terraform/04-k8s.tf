resource "yandex_kubernetes_cluster" "k8s_cluster" {
  name       = var.cluster_name
  network_id = yandex_vpc_network.network_a.id
  master {
    zonal {
      zone      = yandex_vpc_subnet.subnet_a.zone
      subnet_id = yandex_vpc_subnet.subnet_a.id
    }
    public_ip          = true
    security_group_ids = [yandex_vpc_security_group.secgroup_k8s.id]
    maintenance_policy {
      auto_upgrade     = true
      maintenance_window {
        day            = "sunday"
        start_time     = "01:00"
        duration       = "1h"
      }
    }
  }
  service_account_id      = yandex_iam_service_account.sa.id
  node_service_account_id = yandex_iam_service_account.sa.id
    depends_on = [
      yandex_resourcemanager_folder_iam_member.editor,
      yandex_resourcemanager_folder_iam_member.images-puller
    ]
  kms_provider {
    key_id = yandex_kms_symmetric_key.k8s_kms_key.id
  }
}

resource "yandex_kubernetes_node_group" "nodes" {
  cluster_id  = yandex_kubernetes_cluster.k8s_cluster.id
  name        = var.nodes_group_name
  instance_template {
    name            = "node-{instance.index}"
    platform_id     = var.nodes_platform_id
    network_interface {
      nat           = true
      subnet_ids    = ["${yandex_vpc_subnet.subnet_a.id}"]
    }
    resources {
      core_fraction = var.nodes_core_fraction
      memory        = var.nodes_ram
      cores         = var.nodes_cpu
    }
    boot_disk {
        type        = var.nodes_disk_type
        size        = var.nodes_disk_size
    }
    scheduling_policy {
      preemptible   = var.nodes_is_preemtible
    }
    container_runtime {
      type          = "containerd"
    }
    metadata = {
      user-data = "${file("./meta.yml")}"
    }
  }
  scale_policy {
    auto_scale {
      min     = var.min_nodes
      max     = var.max_nodes
      initial = var.initial_nodes
    }
  }
  allocation_policy {
    location {
      zone = var.availibility_zone_a
    }
  }
}

resource "kubernetes_namespace" "namespace_game" {
  metadata {
    name = var.namespace_game
  }
  depends_on = [
    yandex_kubernetes_node_group.nodes
  ]
}
resource "kubernetes_namespace" "namespace_ingress" {
  metadata {
    name = var.namespace_ingress
  }
  depends_on = [
    yandex_kubernetes_node_group.nodes
  ]
}
