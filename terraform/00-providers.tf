terraform {
  required_version = ">= 0.13"
  required_providers {
    yandex   = {
      source = "yandex-cloud/yandex"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
    }
    helm = {
      source  = "hashicorp/helm"
    }
  }

# move terraform.tfstate in bucket
# keys moved in backend.conf. init terraform with --backend-config=backend.conf
  backend "s3" {
    endpoint                    = "https://storage.yandexcloud.net"
    bucket                      = "state2048"
    region                      = "ru-central1"
    key                         = "terraform.tfstate"
    skip_region_validation      = true
    skip_credentials_validation = true
    skip_requesting_account_id  = true
    skip_s3_checksum            = true
  }
}

provider "yandex" {
  service_account_key_file = var.sa_key_file
  cloud_id                 = var.cloud_id
  folder_id                = var.folder_id
  zone                     = var.availibility_zone_a
}

provider "kubernetes" {
  host                   = yandex_kubernetes_cluster.k8s_cluster.master.0.external_v4_endpoint
  cluster_ca_certificate = yandex_kubernetes_cluster.k8s_cluster.master.0.cluster_ca_certificate
  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    args        = ["k8s", "create-token"]
    command     = "yc"
  }
}

provider "helm" {
  kubernetes {
    host                   = yandex_kubernetes_cluster.k8s_cluster.master.0.external_v4_endpoint
    cluster_ca_certificate = yandex_kubernetes_cluster.k8s_cluster.master.0.cluster_ca_certificate
    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      args        = ["k8s", "create-token"]
      command     = "yc"
    }
  }
}
