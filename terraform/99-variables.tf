# provider
variable "sa_key_file" {
  description = "name of service account key file"
  type = string
  default = "key.json"
}
variable "cloud_id" {
  description = "cloud id"
  type = string
}
variable folder_id {
  description = "folder id"
  type = string
}

variable bucket_state {
  description = "bucket name for the tfstate file"
  type = string
}

# sa
variable "sa_name" {
  description = "name of your service account"
  type = string
  default = "sa"
}

# networks
variable "network_a_name" {
  description = "name of network"
  type = string
  default = "network-a"
}
variable "subnet_a_name" {
  description = "name of subnet"
  type = string
  default = "subnet-a"
}
variable "cidr_subnet_a" {
  description = "subnet address"
  type = string
  default = "172.16.0.0/24"
}
variable "availibility_zone_a" {
  description = "availibility zones: ru-central1-a, ru-central1-b, ru-central1-c (will be disabled)"
  type = string
  default = "ru-central1-a"
}
variable "region_var" {
  description = "there is only one region. you don't need to change this variable"
  type = string
  default = "ru-central1"
}
variable "external_address" {
  description = "external address resource name"
  type = string
  default = "ext-address"
}

# security groups
variable "application_port" {
  description = "application port"
  type = number
  default = 80
}
variable "whitelist_ip" {
  description = "ip addresses for which the application can be accessed"
  type = list(string)
  default = ["0.0.0.0/0"]
}
variable "management_ip" {
  description = "ip addresses of management host"
  type = string
}
variable "management_port" {
  description = "ssh port for management"
  type = number
}

# k8s settings
variable "cluster_name" {
  description = "k8s cluster name"
  type = string
  default = "game-cluster"
}
variable "namespace_game" {
  description = "namespace for game"
  type = string
  default = "ns-game"
}
variable "namespace_ingress" {
  description = "namespace for ingress"
  type = string
  default = "ns-ingress"
}

# nodes parameters 
variable "nodes_group_name" {
  description = "nodes group name"
  type = string
  default = "game-group"
}
variable "nodes_platform_id" {
  description = "id of os distributive"
  type = string
  default = "standard-v1"
}
variable "initial_nodes" {
  description = "initial number of nodes"
  type = number
  default = 2
}
variable "min_nodes" {
  description = "minimum number of nodes"
  type = number
  default = 1
}
variable "max_nodes" {
  description = "maximum number of nodes"
  type = number
  default = 3
}
variable "nodes_ram" {
  description = "nodes ram size in Gb"
  type = number
  default = 2
}
variable "nodes_cpu" {
  description = "number of cpu cores for nodes"
  type = number
  default = 2
}
variable "nodes_core_fraction" {
  description = "nodes core fraction of cpu"
  type = number
  default = 20
}
variable "nodes_disk_type" {
  description = "nodes disk type"
  type = string
  default = "network-hdd"
}
variable "nodes_disk_size" {
  description = "nodes disk size in Gb"
  type = number
  default = 30
}
variable "nodes_is_preemtible" {
  description = "create preemtible instances"
  type = bool
  default = true
}

# ingress
variable "ingress_name" {
  description = "ingress name"
  type = string
  default = "ingress-nginx"
}
variable "ingress_chart_repo" {
  description = "url of the helm chart repository for ingress"
  type = string
  default = "https://kubernetes.github.io/ingress-nginx"
}
variable "ingress_chart_name" {
  description = "name of ingress helm chart"
  type = string
  default = "ingress-nginx"
}
variable "ingress_values_path" {
  description = "path to the values file"
  type = string
  default = "./helm/ingress/values.yaml"
}

# game deployment
variable "game_chart_name" {
  description = "name of game helm chart"
  type = string
  default = "game-chart"
}
variable "game_chart_path" {
  description = "path to the game chart"
  type = string
  default = "./helm/game"
}
variable "game_values_path" {
  description = "path to the values file"
  type = string
  default = "./helm/game/values.yaml"
}
variable "game_service_name" {
  description = "name of the service for game"
  type = string
  default = "service-game"
}
