resource "yandex_vpc_network" "network_a" {
  name        = var.network_a_name
}

resource "yandex_vpc_subnet" "subnet_a" {
  name           = var.subnet_a_name
  zone           = var.availibility_zone_a
  network_id     = yandex_vpc_network.network_a.id
  v4_cidr_blocks = [var.cidr_subnet_a]
}
